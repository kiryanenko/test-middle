function pluralize(number, one, two, five) {
  let n = Math.abs(number);
  n %= 100;
  if (n >= 5 && n <= 20) {
      return five;
  }
  n %= 10;
  if (n === 1) {
      return one;
  }
  if (n >= 2 && n <= 4) {
      return two;
  }
  return five;
}

new Promise((resolve, reject) => {
  setTimeout(() => {
    const candidate = {
      name: 'Александр',
      age: 24,
      job: {
        company: 'Открытая Школа',
        title: 'Разработчик'
      },
      expertise: [
        {
          workName: 'EdCrunch',
          timeInMonth: 36
        },
        {
          workName: 'Mail.Ru',
          timeInMonth: 27
        },
      ]
    };
    resolve(candidate);
  }, 2000)
}).then(candidate => {
  const {age, name, job, expertise} = candidate;
  const yearOfBirth = 2020 - age;

  const totalExperience = expertise.reduce((sum, job) => sum + job.timeInMonth, 0);

  let experienceStr;
  if (totalExperience === 0) {
    experienceStr = 'менее 1 месяца';
  } else if (totalExperience < 12) {
    experienceStr = `более ${totalExperience} ${pluralize(totalExperience, 'месяца', 'месяцев', 'месяцев')}`;
  } else {
    const experienceInYears = Math.floor(totalExperience / 12);
    experienceStr = `более ${experienceInYears} ${pluralize(experienceInYears, 'года', 'лет', 'лет')}`;
  }
  
  const text = `${name} сейчас работает в компании ${job.company} и родился в ${yearOfBirth} году. Его опыт работы ${experienceStr}.`;
  console.log(text);
}).catch(error => {
  console.error(error);
});